import React, { Component, Fragment } from 'react';
//import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import Hidden from '@material-ui/core/Hidden';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
//import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  grow: {
    flexGrow: 1,
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    flexShrink: 0,
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: 0,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: drawerWidth,
  },
  card: {
    minWidth: 275,
    margin: "0px 0px 16px 0px",
  },
  title: {
    fontSize: 14,
  },
  goToRight: {
    marginRight: 20,
  },
});

class App extends Component {
  state = {
    open: false,
    wasOpen: false,
    courseDates: [
      { id: '1', name: 'Factor Based Investing Course', date: '23 February 2019 to 25 February 2019' },
      { id: '2', name: 'Early Retirement Masterclass', date: '16 February 2019 to 17 February 2019' },
      { id: '3', name: 'CryptoKnight Masterclass', date: '19 January 2019 to 20 January 2019' }
    ],
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  }

  handleDrawerToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, theme } = this.props;
    const { open } = this.state;

    const drawer = (
      <Fragment>
        <List>
          <Link to="/Customers" style={{ textDecoration: 'none' }}>
            <ListItem button key='Customers'>
              <ListItemText primary='Customers' />
            </ListItem>
          </Link>
          <Link to="/Courses" style={{ textDecoration: 'none' }}>
            <ListItem button key='Courses'>
              <ListItemText primary='Courses' />
            </ListItem>
          </Link>
        </List>
        <Divider />
      </Fragment>
    );

    const homeTitle = ({ match }) => (
      <Typography variant="h6" color="inherit" noWrap>
        Dashboard
      </Typography>
    );

    const customersTitle = ({ match }) => (
      <Typography variant="h6" color="inherit" noWrap>
        Customers
      </Typography>
    );

    const coursesTitle = ({ match }) => (
      <Typography variant="h6" color="inherit" noWrap>
        Courses
      </Typography>
    );

    const homeContent = ({ match }) => (
      <Fragment>
        <Typography paragraph>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Rhoncus dolor purus non enim praesent
          elementum facilisis leo vel. Risus at ultrices mi tempus imperdiet. Semper risus in
          hendrerit gravida rutrum quisque non tellus. Convallis convallis tellus id interdum
          velit laoreet id donec ultrices. Odio morbi quis commodo odio aenean sed adipiscing.
          Amet nisl suscipit adipiscing bibendum est ultricies integer quis. Cursus euismod quis
          viverra nibh cras. Metus vulputate eu scelerisque felis imperdiet proin fermentum leo.
          Mauris commodo quis imperdiet massa tincidunt. Cras tincidunt lobortis feugiat vivamus
          at augue. At augue eget arcu dictum varius duis at consectetur lorem. Velit sed
          ullamcorper morbi tincidunt. Lorem donec massa sapien faucibus et molestie ac.
          </Typography>
        <Typography paragraph>
          Consequat mauris nunc congue nisi vitae suscipit. Fringilla est ullamcorper eget nulla
          facilisi etiam dignissim diam. Pulvinar elementum integer enim neque volutpat ac
          tincidunt. Ornare suspendisse sed nisi lacus sed viverra tellus. Purus sit amet volutpat
          consequat mauris. Elementum eu facilisis sed odio morbi. Euismod lacinia at quis risus
          sed vulputate odio. Morbi tincidunt ornare massa eget egestas purus viverra accumsan in.
          In hendrerit gravida rutrum quisque non tellus orci ac. Pellentesque nec nam aliquam sem
          et tortor. Habitant morbi tristique senectus et. Adipiscing elit duis tristique
          sollicitudin nibh sit. Ornare aenean euismod elementum nisi quis eleifend. Commodo
          viverra maecenas accumsan lacus vel facilisis. Nulla posuere sollicitudin aliquam
          ultrices sagittis orci a.
          </Typography>
      </Fragment>
    );

    const customersContent = ({ match }) => (
      <Fragment>
        <Typography title>
          CUSTOMERS
        </Typography>
        <br />
        <Typography paragraph>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Rhoncus dolor purus non enim praesent
          elementum facilisis leo vel. Risus at ultrices mi tempus imperdiet. Semper risus in
          hendrerit gravida rutrum quisque non tellus. Convallis convallis tellus id interdum
          velit laoreet id donec ultrices. Odio morbi quis commodo odio aenean sed adipiscing.
          Amet nisl suscipit adipiscing bibendum est ultricies integer quis. Cursus euismod quis
          viverra nibh cras. Metus vulputate eu scelerisque felis imperdiet proin fermentum leo.
          Mauris commodo quis imperdiet massa tincidunt. Cras tincidunt lobortis feugiat vivamus
          at augue. At augue eget arcu dictum varius duis at consectetur lorem. Velit sed
          ullamcorper morbi tincidunt. Lorem donec massa sapien faucibus et molestie ac.
          </Typography>
      </Fragment>
    );

    const coursesContent = ({ match }) => (
      <Fragment>
        {this.state.courseDates.map((course, index) => (
          <Link to="/Home" style={{ textDecoration: 'none' }}>
            <Card className={classes.card}>
              <Toolbar disableGutters>
                <CardContent className={classes.grow}>
                  <Typography variant="subheading">
                    {course.name}
                  </Typography>
                  <Typography color="textSecondary">
                    {course.date}
                  </Typography>
                </CardContent>
                <div className={classes.goToRight}>
                  <ChevronRightIcon />
                </div>
              </Toolbar>
            </Card>
          </Link>
        ))}
      </Fragment>
    )

    return (
      <Router>
        <Fragment>
          <Hidden mdUp>
            <AppBar position="fixed">
              <Toolbar>
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  onClick={this.handleDrawerOpen}
                  className={classNames(classes.menuButton, open && classes.hide)}
                >
                  <MenuIcon />
                </IconButton>
                <Drawer
                  className={classes.drawer}
                  variant="temporary"
                  anchor="left"
                  open={open}
                  onClose={this.handleDrawerToggle}
                  classes={{
                    paper: classes.drawerPaper,
                  }}>
                  <div className={classes.toolbar} />
                  <Divider />
                  <div onClick={this.handleDrawerToggle}>
                    {drawer}
                  </div>
                </Drawer>
                <Route exact path="/" component={homeTitle} />
                <Route path="/Customers" component={customersTitle} />
                <Route path="/Courses" component={coursesTitle} />
              </Toolbar>
            </AppBar>
            <main
              className={classNames(classes.content)}
            >
              <div className={classes.drawerHeader} />
              <Route exact path="/" component={homeContent} />
              <Route path="/Customers" component={customersContent} />
              <Route path="/Courses" component={coursesContent} />
            </main>
          </Hidden>
          <Hidden smDown>
            <AppBar position="fixed"
              className={classNames(classes.appBar, {
                [classes.appBarShift]: open,
              })}
            >
              <Toolbar>
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  onClick={this.handleDrawerOpen}
                  className={classNames(classes.menuButton, open && classes.hide)}
                >
                  <MenuIcon />
                </IconButton>
                <Drawer
                  className={classes.drawer}
                  variant="persistent"
                  anchor="left"
                  open={open}
                  classes={{
                    paper: classes.drawerPaper,
                  }}
                >
                  <div className={classes.drawerHeader}>
                    <IconButton onClick={this.handleDrawerClose}>
                      {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                  </div>
                  <Divider />
                  {drawer}
                </Drawer>
                <Route exact path="/" component={homeTitle} />
                <Route path="/Customers" component={customersTitle} />
                <Route path="/Courses" component={coursesTitle} />
              </Toolbar>
            </AppBar>
            <main
              className={classNames(classes.content, {
                [classes.contentShift]: open,
              })}
            >
              <div className={classes.drawerHeader} />
              <Route exact path="/" component={homeContent} />
              <Route path="/Customers" component={customersContent} />
              <Route path="/Courses" component={coursesContent} />
            </main>
          </Hidden>
        </Fragment>
      </Router>
    );
  }
}

export default withStyles(styles, { withTheme: true })(App);